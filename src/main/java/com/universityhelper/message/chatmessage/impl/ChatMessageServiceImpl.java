package com.universityhelper.message.chatmessage.impl;

import com.universityhelper.message.chatmessage.ChatMessage;
import com.universityhelper.message.chatmessage.ChatMessageInfo;
import com.universityhelper.message.chatmessage.ChatMessageService;
import com.universityhelper.message.chatmessage.ChatSendMessage;
import com.universityhelper.message.chatmessage.dao.ChatMessageDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class ChatMessageServiceImpl implements ChatMessageService {

    @Resource
    private ChatMessageDao chatMessageDao;

    @Override
    public List<ChatMessage> getMessageList(String userID,String friendID) {
        List<ChatMessage> list = chatMessageDao.getMessageList(userID,friendID);
        return list;
    }

    @Override
    public void sendMessage(ChatSendMessage chatSendMessage) {
        chatMessageDao.sendMessage1(chatSendMessage.getSender_id(),chatSendMessage.getReceiver_id(), chatSendMessage.getMessage_content(),chatSendMessage.getContent_type());
        chatMessageDao.sendMessage2(chatSendMessage.getSender_id(),chatSendMessage.getReceiver_id(), chatSendMessage.getMessage_content(),chatSendMessage.getContent_type());
    }

    @Override
    public void delMessage(String id) {
        chatMessageDao.delMessage(id);
    }
}
