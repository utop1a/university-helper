package com.universityhelper.message.chatmessage.dao;

import com.universityhelper.message.chatmessage.ChatMessage;

import java.util.List;

public interface ChatMessageDao {

    List<ChatMessage> getMessageList(String userID,String friendID);
    void sendMessage1(String senderID,String receiverID,String content,Integer content_type);
    void sendMessage2(String senderID,String receiverID,String content,Integer content_type);
    void delMessage(String id);
}
