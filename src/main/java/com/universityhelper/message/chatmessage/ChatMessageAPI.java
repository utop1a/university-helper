package com.universityhelper.message.chatmessage;

import com.universityhelper.common.CommonService;
import com.universityhelper.common.CurrentUser;
import com.universityhelper.common.Result;
import com.universityhelper.common.TokenUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController//说明这是一个受spring容器管理的控制器组件(Bean)
@RequestMapping("/chatMessage")//本控制器的路径前缀
public class ChatMessageAPI {

    @Resource
    private ChatMessageService chatMessageService;

    @GetMapping("/getMessageList")
    public Result getChatList(String userID,String friendID){
        System.out.println(userID);
        System.out.println(friendID);
        List<ChatMessage> list = chatMessageService.getMessageList(userID,friendID);
        return Result.success("成功",list);
    }

    @PostMapping("/sendMessage")
    public Result sendMessage(ChatSendMessage chatSendMessage){
        chatMessageService.sendMessage(chatSendMessage);
        return Result.success("成功");
    }

    @GetMapping("/delMessage")
    public Result delMessage(@RequestHeader("Token") String token, String id){
        System.out.println(id);
        chatMessageService.delMessage(id);

        return Result.success("成功");
    }


}

