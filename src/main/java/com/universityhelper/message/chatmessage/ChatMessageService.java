package com.universityhelper.message.chatmessage;

import java.util.List;

public interface ChatMessageService {
    List<ChatMessage> getMessageList(String userID,String friendID);
    void sendMessage(ChatSendMessage chatSendMessage);
    void delMessage(String id);
}

