package com.universityhelper.message.chat;

import com.universityhelper.common.CurrentUser;

import java.util.List;

public interface ChatService {

    List<Chat> getChatList(String userID);
    void delChat(String user_id,String friend_id);
    boolean addChat(String user_id,String friend_id);
}
