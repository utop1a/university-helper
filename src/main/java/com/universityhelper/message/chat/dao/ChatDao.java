package com.universityhelper.message.chat.dao;

import com.universityhelper.message.chat.Chat;
import java.util.List;

public interface ChatDao {
    List<Chat> getChatList(String userID);
    void delChat(String userID,String friendID);
    void delAllMessages(String userID,String friendID);
    void addChat1(String userID,String friendID);
    void addChat2(String userID,String friendID);
    void addMessage1(String userID,String friendID);
    void addMessage2(String userID,String friendID);
}
