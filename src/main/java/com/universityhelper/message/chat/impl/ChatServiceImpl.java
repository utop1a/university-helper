package com.universityhelper.message.chat.impl;

import com.universityhelper.common.CurrentUser;
import com.universityhelper.message.chat.Chat;
import com.universityhelper.message.chat.ChatService;
import com.universityhelper.message.chat.dao.ChatDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class ChatServiceImpl implements ChatService {

    @Resource
    private ChatDao chatDao;

    @Override
    public List<Chat> getChatList(String userID) {
        System.out.println(userID);
        return chatDao.getChatList(userID);
    }

    @Override
    public void delChat(String userID, String friendID) {
        chatDao.delChat(userID,friendID);
        chatDao.delAllMessages(userID,friendID);
    }

    @Override
    public boolean addChat(String userID, String friendID) {
        List<Chat> list = chatDao.getChatList(userID);
        for (Chat chat : list) {
            if(chat.getFriend_id().equals(friendID)){
                System.out.println("iuahdilasuidis48a86da1da61sds");
                return false;
            }
        }
        System.out.println("iuahdilasuidis");
        chatDao.addChat1(userID,friendID);
        chatDao.addChat2(userID,friendID);
        chatDao.addMessage1(userID,friendID);
        chatDao.addMessage2(userID,friendID);
        return true;
    }
}
