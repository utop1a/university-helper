package com.universityhelper.message.chat;

import com.universityhelper.common.CommonService;
import com.universityhelper.common.CurrentUser;
import com.universityhelper.common.Result;
import com.universityhelper.common.TokenUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController//说明这是一个受spring容器管理的控制器组件(Bean)
@RequestMapping("/chat")//本控制器的路径前缀
public class ChatAPI {

    //引入依赖对象
    @Resource//此注解表示从容器中引入依赖对象
    private CommonService commonService;
    @Resource
    private ChatService chatService;

    @GetMapping("/getChatList")
    public Result getChatList(String userID){
        System.out.println("准备执行Service");
        List<Chat> list = chatService.getChatList(userID);
        System.out.println("执行Service结束");
        System.out.println(list);

        return Result.success("成功",list);
    }

    @GetMapping("/addChat")
    public Result addChat(String userID,String friendID){
        boolean b = chatService.addChat(userID, friendID);
        if(b){
            return Result.success("添加成功");
        }
        return Result.success("重复添加");

    }

    @GetMapping("/delChat")
    public Result delChat(String user_id,String friend_id){

        chatService.delChat(user_id,friend_id);

        return Result.success("删除成功");
    }

}
