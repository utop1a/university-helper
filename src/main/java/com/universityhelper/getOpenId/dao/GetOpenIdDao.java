package com.universityhelper.getOpenId.dao;

import com.universityhelper.getOpenId.UserInfoDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Mapper
public interface GetOpenIdDao {

    @Insert("insert into user(userID,avatar,userName) values(#{user_id},#{avatar},#{name})")
    void userAdd(UserInfoDto userInfoDto);
}
