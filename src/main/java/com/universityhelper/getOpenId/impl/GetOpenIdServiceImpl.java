package com.universityhelper.getOpenId.impl;

import com.universityhelper.getOpenId.GetOpenIdService;
import com.universityhelper.getOpenId.UserInfoDto;
import com.universityhelper.getOpenId.dao.GetOpenIdDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class GetOpenIdServiceImpl implements GetOpenIdService {
    @Resource
    private GetOpenIdDao getOpenIdDao;

    @Override
    public void userAdd(UserInfoDto userInfoDto) {
        getOpenIdDao.userAdd(userInfoDto);
    }
}
