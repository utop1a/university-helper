package com.universityhelper.getOpenId;


import com.universityhelper.common.HttpRequest;
import com.universityhelper.common.Result;

import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class getOpenIdAPI {
    @Resource
    private GetOpenIdService getOpenIdService;

    @GetMapping("/decodeUserInfo")
    public Result decodeUserInfo(String code) {

        // 小程序唯一标识 (在微信小程序管理后台获取)
        String wxspAppid = "wxff59eb1129e339f6";
        // 小程序的 app secret (在微信小程序管理后台获取)
        String wxspSecret = "a1189ea951dffdd60c8c555be5477773";
        // 授权（必填）
        String grant_type = "authorization_code";

        // 请求参数
        String params = "appid=" + wxspAppid + "&secret=" + wxspSecret + "&js_code=" + code + "&grant_type="
                + grant_type;
        // 发送请求
        String sr = HttpRequest.sendGet("https://api.weixin.qq.com/sns/jscode2session", params);
        // 解析相应内容（转换成json对象）
        JSONObject json =JSONObject.fromObject((String)sr);
        // 用户的唯一标识（openid）
        String openid = (String) json.get("openid");
        System.out.println("openid: " +openid);
      return Result.success((Object)openid);

    }

    @PostMapping("/userAdd")
    public Result favorite(@RequestBody UserInfoDto userInfoDto){
        getOpenIdService.userAdd(userInfoDto);
        return Result.success();
    }
}
