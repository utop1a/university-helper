package com.universityhelper.home.impl;

import com.universityhelper.home.*;
import com.universityhelper.home.dao.HomeDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class HomeServiceImpl implements HomeService {
    @Resource
    private HomeDao homeDao;

    @Override
    public List<OrderDto> getAllOrder() {
        List<OrderDto> orderList=homeDao.getAllOrder();
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(homeDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public List<OrderDto> getCategoryOrder(String category) {
        List<OrderDto> orderList=homeDao.getCategoryOrder(category);
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(homeDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public List<OrderDto> getSearchOrder(SearchDto searchDto)
    {
        searchDto.setSearchID(homeDao.getMaxSearchID()+1);
        homeDao.addSearchHistory(searchDto);
        List<OrderDto> orderList=homeDao.getSearchOrder(searchDto.getSearch());
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(homeDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public List<String> getSearchHistory(String userID) {
        return homeDao.getSearchHistory(userID);
    }

    @Override
    public DetailDto getDetail(UserProductionDto userProductionDto) {
        String userID=userProductionDto.getUserID();
        String productionID=userProductionDto.getProductionID();
        DetailDto detailDto=homeDao.getDetail(productionID);
        detailDto.setImage(homeDao.getImageList(productionID));
        if(homeDao.getBrowsingHistory(userProductionDto)>0) {
            homeDao.updateBrowsingTime(userProductionDto);
        }
        else {
            System.out.println("ssssssss");
            homeDao.insertBrosingHistory(userProductionDto);
        }
        detailDto.setRecieverName(homeDao.getRecieverID(detailDto.getRecieverID()));
        return detailDto;
    }

    @Override
    public void addComment1(Comment1Dto comment1Dto) {
        comment1Dto.setCommentID(homeDao.getMaxComment1ID()+1);
        homeDao.addComment1(comment1Dto);
    }

    @Override
    public void addComment2(Comment2Dto comment2Dto) {
        comment2Dto.setCommentID(homeDao.getMaxComment2ID()+1);
        homeDao.addComment2(comment2Dto);
    }

    @Override
    public void delComment1(int commentID) {
        homeDao.delComment1(commentID);
        homeDao.delComment2All(commentID);
    }

    @Override
    public void delComment2(int commentID) {
        homeDao.delComment2(commentID);
    }

    @Override
    public List<FatherCommentDto> getFatherComment(String productionID) {
        List<FatherCommentDto> fatherCommentDtoList=homeDao.getFatherCommentList(productionID);
        for(int i=0;i<fatherCommentDtoList.size();i++) {
        fatherCommentDtoList.get(i).setChildrenCommentNum(homeDao.getCount(fatherCommentDtoList.get(i).getCommentID()));
        }
        return fatherCommentDtoList;
    }

    @Override
    public List<ChildrenCommentDto> getChildrenComment(int commentID) {
        List<ChildrenCommentDto> childrenCommentDtoList=homeDao.getChildrenCommentList(commentID);
        for(int i=0;i<childrenCommentDtoList.size();i++) {
            ChildrenCommentDto c=childrenCommentDtoList.get(i);
            if(c.getUserID2().equals("0")) {
                continue;
            }
            else {
                c.setUserName2(homeDao.getUserName(c.getUserID2()));
            }
        }
        return childrenCommentDtoList;
    }

    @Override
    public List<String> getCategoryList() {
        return homeDao.getCategoryList();
    }

    @Override
    public int getCollect(CollectDto collectDto) {
        return homeDao.getCollect(collectDto);
    }

    @Override
    public void doCollect(CollectDto collectDto) {
        homeDao.doCollect(collectDto);
    }

    @Override
    public void cancelCollect(CollectDto collectDto) {
        homeDao.cancelCollect(collectDto);
    }

    @Override
    public void buy(CollectDto collectDto) {
        homeDao.buy(collectDto);
    }

    @Override
    public String getBuy(String productionID) {
        int num=homeDao.getBuy(productionID);
        if(num==1) {
            return "0";
        }
        else {
            return "1";
        }
    }
}
