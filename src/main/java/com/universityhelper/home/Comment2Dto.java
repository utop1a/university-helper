package com.universityhelper.home;

public class Comment2Dto {
    String userID;
    String userID2;
    String productionID;
    String content;
    int fatherCommentID;
    int commentID;

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID2() {
        return userID2;
    }

    public void setUserID2(String userID2) {
        this.userID2 = userID2;
    }

    public String getProductionID() {
        return productionID;
    }

    public void setProductionID(String productionID) {
        this.productionID = productionID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getFatherCommentID() {
        return fatherCommentID;
    }

    public void setFatherCommentID(int fatherCommentID) {
        this.fatherCommentID = fatherCommentID;
    }
}
