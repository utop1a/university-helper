package com.universityhelper.home;

import java.util.List;

public class CommentDto {
    FatherCommentDto fatherCommentDto;

    public FatherCommentDto getFatherCommentDto() {
        return fatherCommentDto;
    }

    public void setFatherCommentDto(FatherCommentDto fatherCommentDto) {
        this.fatherCommentDto = fatherCommentDto;
    }

    public List<ChildrenCommentDto> getChildrenCommentDtoList() {
        return childrenCommentDtoList;
    }

    public void setChildrenCommentDtoList(List<ChildrenCommentDto> childrenCommentDtoList) {
        this.childrenCommentDtoList = childrenCommentDtoList;
    }

    List<ChildrenCommentDto> childrenCommentDtoList;
    public CommentDto(FatherCommentDto f) {
        fatherCommentDto=f;
    }
}
