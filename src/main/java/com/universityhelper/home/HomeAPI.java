package com.universityhelper.home;

import com.universityhelper.common.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/home")
public class HomeAPI {
    @Resource
    private HomeService homeService;

    @GetMapping("/all")
    public Result getAllOrder(){
        return Result.success(homeService.getAllOrder());
    }
    @GetMapping("/category")
    public Result getCategoryOrder(String category){

        return Result.success(homeService.getCategoryOrder(category));
    }
    @GetMapping("/search")
    public Result getSearchOrder(SearchDto searchDto){
        return Result.success(homeService.getSearchOrder(searchDto));
    }

    @GetMapping("/history")
    public Result getHistory(String userID){
        return Result.success(homeService.getSearchHistory(userID));
    }

    @GetMapping("/detail")
    public Result getOrderDetail(UserProductionDto userProductionDto){
        return Result.success(homeService.getDetail(userProductionDto));
    }

    @PostMapping("/addComment1")
    public Result addComment1(@RequestBody Comment1Dto comment1Dto) {
        System.out.println("我是黄明浪");
        homeService.addComment1(comment1Dto);
        return Result.success();
    }

    @PostMapping("/addComment2")
    public Result addComment2(@RequestBody Comment2Dto comment2Dto) {
        homeService.addComment2(comment2Dto);
        return Result.success();
    }

    @PostMapping("/delComment1")
    public Result delComment1(@RequestBody Comment1Dto comment1Dto) {
        homeService.delComment1(comment1Dto.getCommentID());
        return Result.success();
    }

    @PostMapping("/delComment2")
    public Result delComment2(@RequestBody Comment1Dto comment2Dto) {
        homeService.delComment2(comment2Dto.getCommentID());
        return Result.success();
    }

    @GetMapping("/getFatherComment")
    public Result getFatherComment(String productionID) {
        return Result.success(homeService.getFatherComment(productionID));
    }

    @GetMapping("/getChildrenComment")
    public Result getChildrenComment(Comment1Dto comment1Dto) {
        return Result.success(homeService.getChildrenComment(comment1Dto.getCommentID()));
    }

    @GetMapping("/getCategoryList")
    public Result getCategoryList() {
        return Result.success(homeService.getCategoryList());
    }

    @GetMapping("/getCollect")
    public Result getCollect(CollectDto collectDto) {
        return Result.success((Object) Integer.toString(homeService.getCollect(collectDto)));
    }

    @PostMapping("/doCollect")
    public Result doCollect(@RequestBody CollectDto collectDto) {
        homeService.doCollect(collectDto);
        return Result.success();
    }
    @PostMapping("/cancelCollect")
    public Result cancelCollect(@RequestBody CollectDto collectDto) {
        homeService.cancelCollect(collectDto);
        return Result.success();
    }

    @PostMapping("/buy")
    public Result buy(@RequestBody CollectDto collectDto) {
        homeService.buy(collectDto);
        return Result.success();
    }

    @GetMapping("/getBuy")
    public Result getBuy(String productionID) {
        return Result.success((Object) homeService.getBuy(productionID));
    }
}
