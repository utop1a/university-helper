package com.universityhelper.home.dao;

import com.universityhelper.home.*;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface HomeDao {
    @Select("select image from image where productionID=#{productionID} limit 1")
    String getImage(String productionID);

    @Select("select user.userName from user where user.userID=#{recieverID}")
    String getRecieverID(String recieverID);
    @Select("select price,title,productionID,description from production order by dateTime desc")
    List<OrderDto> getAllOrder();

    @Select("select price,title,production.productionID,description from production where  category=#{category}")
    List<OrderDto> getCategoryOrder(String category);

    @Select("select price,title,production.productionID,description from production where (production.title LIKE concat('%',#{search},'%') OR production.description LIKE concat('%',#{search},'%') OR production.category LIKE concat('%',#{search},'%'))")
    List<OrderDto> getSearchOrder(String search);

    @Select("select content from searchHistory where userID=#{userID}")
    List<String> getSearchHistory(String userID);

    @Select("select avatar,userName,user.userID,title,price,description,dateTime,recieverID,status from production,user where user.userID=production.userID and productionID=#{productionID}")
    DetailDto getDetail(String productionID);

    @Select("select image from image where productionID=#{productionID}")
    List<String> getImageList(String productionID);

    @Select("select count(*) from browsingHistory where userID=#{userID} and productionID=#{productionID}")
    Integer getBrowsingHistory(UserProductionDto userProductionDto);

    @Update("update browsingHistory set time=now() where userID=#{userID} and productionID=#{productionID} ")
    void updateBrowsingTime(UserProductionDto userProductionDto);

    @Insert("insert into browsingHistory(time,productionID,userID) values(now(),#{productionID},#{userID})")
    void insertBrosingHistory(UserProductionDto userProductionDto);

    @Select("select max(commentID) from comment1")
    int getMaxComment1ID();

    @Insert("insert into comment1(commentID,productionID,content,time,userID) values(#{commentID},#{productionID},#{content},now(),#{userID})")
    void addComment1(Comment1Dto comment1Dto);

    @Select("select max(commentID) from comment2")
    int getMaxComment2ID();

    @Insert("insert into comment2(commentID,productionID,content,time,userID,userID2,fatherCommentID) values(#{commentID},#{productionID},#{content},now(),#{userID},#{userID2},#{fatherCommentID})")
    void addComment2(Comment2Dto comment2Dto);

    @Delete("delete from comment1 where commentID=#{commentID}")
    void delComment1(int commentID);

    @Delete("delete from comment2 where fatherCommentID=#{commentID}")
    void delComment2All(int commentID);

    @Delete("delete from comment2 where commentID=#{commentID}")
    void delComment2(int commentID);

    @Select("select count(*) from comment2 where fatherCommentID=#{commentID}")
    int getCount(int commentID);

    @Select("select avatar,userName,content,time,commentID,user.userID from user,comment1 where productionID=#{productionID} and comment1.userID=user.userID")
    List<FatherCommentDto> getFatherCommentList(String production);

    @Select("select userName from user where userID=#{userID}")
    String getUserName(String userID);

    @Select("select avatar,userName,content,time,commentID,user.userID,userID2 from user,comment2 where fatherCommentID=#{commentID} and user.userID=comment2.userID")
    List<ChildrenCommentDto> getChildrenCommentList(int commentID);

    @Select("select category from category")
    List<String> getCategoryList();

    @Select("select count(*) from collect where userID=#{userID} and productionID=#{productionID}")
    int getCollect(CollectDto collectDto);

    @Insert("insert into collect(userID,productionID,time) values(#{userID},#{productionID},now())")
    void doCollect(CollectDto collectDto);

    @Delete("delete from collect where userID=#{userID} and productionID=#{productionID}")
    void cancelCollect(CollectDto collectDto);

    @Update("update production set recieverID=#{userID},status=1 where productionID=#{productionID}")
    void buy(CollectDto collectDto);

    @Select("select count(*) from production where productionID=#{productionID} and status=0")
    int getBuy(String productionID);

    @Insert("insert into searchHistory(content,userID,time,searchID) values(#{search},#{userID},now(),#{searchID})")
    void addSearchHistory(SearchDto searchDto);

    @Select("select max(searchID) from searchHistory")
    int getMaxSearchID();
}
