package com.universityhelper.home;

import com.universityhelper.common.Result;

import java.util.List;

public interface HomeService {
    List<OrderDto> getAllOrder() ;
    List<OrderDto> getCategoryOrder(String categoty);
    List<OrderDto> getSearchOrder(SearchDto searchDto);
    List<String> getSearchHistory(String userID);
    DetailDto getDetail(UserProductionDto userProductionDto);
    void addComment1(Comment1Dto comment1Dto);
    void addComment2(Comment2Dto comment2Dto);
    void delComment1(int commentID);
    void delComment2(int commentID);
    List<FatherCommentDto> getFatherComment(String productionID);
    List<ChildrenCommentDto> getChildrenComment(int commentID);
    List<String> getCategoryList();
    int getCollect(CollectDto collectDto);
    void doCollect(CollectDto collectDto);
    void cancelCollect(CollectDto collectDto);
    void buy(CollectDto collectDto);
    String getBuy(String productionID);
}
