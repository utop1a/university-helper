package com.universityhelper.home;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class FatherCommentDto {
    String avatar;
    String userName;
    String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date time;
    int commentID;
    String userID;
    int childrenCommentNum;
    List<ChildrenCommentDto> childrenCommentDtoList=null;

    public int getChildrenCommentNum() {
        return childrenCommentNum;
    }

    public void setChildrenCommentNum(int childrenCommentNum) {
        this.childrenCommentNum = childrenCommentNum;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public List<ChildrenCommentDto> getChildrenCommentDtoList() {
        return childrenCommentDtoList;
    }

    public void setChildrenCommentDtoList(List<ChildrenCommentDto> childrenCommentDtoList) {
        this.childrenCommentDtoList = childrenCommentDtoList;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
