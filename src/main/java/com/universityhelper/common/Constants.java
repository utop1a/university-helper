package com.universityhelper.common;

public class Constants {

    /**
     * 默认token参数名
     */
    private static final String DEFAULT_HEADER_TOKEN = "Token";

    /**
     * 当前token参数名
     */
    public static final String HEADER_TOKEN=tokenHeaderName();

    private static String tokenHeaderName() {
        String headerName =  CommonConfig.getEnv().getProperty("sure.token.header-name");
        return headerName==null?DEFAULT_HEADER_TOKEN:headerName;
    }

}
