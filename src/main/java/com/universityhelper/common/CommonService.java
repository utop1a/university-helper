package com.universityhelper.common;


import com.universityhelper.common.model.User.User;

public interface CommonService {
    User getUserById(String userId);
}
