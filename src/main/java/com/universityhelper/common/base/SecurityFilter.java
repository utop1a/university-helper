package com.universityhelper.common.base;


import com.universityhelper.common.*;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter("/*")
public class SecurityFilter implements Filter {

    @Resource
    private CommonService commonService;



    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String path = request.getServletPath();
        if(path.startsWith("/start")){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }
        else if(path.startsWith("/chat")){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }
        else if(path.startsWith("/gethello")){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }

        String clientToken = request.getHeader(Constants.HEADER_TOKEN);

        try {
            TokenUtils.verifyToken(clientToken,commonService);
            filterChain.doFilter(servletRequest,servletResponse);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.outJson(response, Result.fail(Result.ERR_CODE_UNLOGIN,e.getMessage()));

        }
    }

    @Override
    public void destroy() {

    }
}
