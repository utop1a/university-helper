package com.universityhelper.common.dao;

import com.universityhelper.common.model.User.User;
import org.apache.ibatis.annotations.Select;


public interface CommonDao {

    @Select("select user_id userId,name userName,avatar avatar,code userPwd  from user where user_id = #{userId}")
    User findUserById(String userId);
}
