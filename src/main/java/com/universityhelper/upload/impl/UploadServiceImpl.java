package com.universityhelper.upload.impl;

import com.universityhelper.getOpenId.GetOpenIdService;
import com.universityhelper.getOpenId.UserInfoDto;
import com.universityhelper.getOpenId.dao.GetOpenIdDao;
import com.universityhelper.upload.UploadService;
import com.universityhelper.upload.dao.UploadDao;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class UploadServiceImpl implements UploadService {
    @Resource
    private UploadDao uploadDao;

    @Override
    public void addImage(String image,String productionID) {
        image="http://49.235.98.215:80/image/"+image;
       int id=uploadDao.getMaxImageID()+1;
       System.out.println(image+"  "+id);
       String imageID=Integer.toString(id);
       uploadDao.addImage(image,productionID,imageID);
    }

}
