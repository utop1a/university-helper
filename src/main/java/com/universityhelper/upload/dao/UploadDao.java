package com.universityhelper.upload.dao;

import com.universityhelper.getOpenId.UserInfoDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UploadDao {

@Select("select max(cast(imageID as SIGNED)) from image")
int getMaxImageID();

@Insert("insert into image(imageID,image,productionID) values(#{imageID},#{image},#{productionID})")
    void addImage(String image,String productionID,String imageID);
}
