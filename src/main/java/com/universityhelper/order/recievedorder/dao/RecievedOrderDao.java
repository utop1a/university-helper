package com.universityhelper.order.recievedorder.dao;

import com.universityhelper.order.recievedorder.RecievedOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RecievedOrderDao {
    @Select("select productionID,title from production where recieverID=#{usrID}")
    List<RecievedOrder> getOrderByID(String usrID);

    @Select("select image from image where productionID=#{productionID} limit 1")
    String getImage(String productionID);

    @Select("select productionID,title,dateTime,description,status from production where productionID=#{productionID} limit 1")
    RecievedOrder getRecievedOrderById(String productionID);
}

