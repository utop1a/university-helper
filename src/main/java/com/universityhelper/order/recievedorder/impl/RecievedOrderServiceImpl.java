package com.universityhelper.order.recievedorder.impl;

import com.universityhelper.order.recievedorder.RecievedOrder;
import com.universityhelper.order.recievedorder.RecievedOrderService;
import com.universityhelper.order.recievedorder.dao.RecievedOrderDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class RecievedOrderServiceImpl implements RecievedOrderService {
    @Resource
    private RecievedOrderDao recievedOrderDao;

    @Override
    public List<RecievedOrder> getList(String usrID) {
        List<RecievedOrder> orderList= recievedOrderDao.getOrderByID(usrID);
        for(int i=0;i<orderList.size();i++) {
            System.out.println("------"+orderList.get(i).getProductionID());
            orderList.get(i).setImage(recievedOrderDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public RecievedOrder getRecievedOrderById(String ProductId) {
        return this.recievedOrderDao.getRecievedOrderById(ProductId);
    }
}

