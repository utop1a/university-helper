package com.universityhelper.order.recievedorder;

import com.universityhelper.common.Result;
import com.universityhelper.order.recievedorder.impl.RecievedOrderServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class RecievedOrderAPI {
    @Resource
    RecievedOrderService recievedOrderService= new RecievedOrderServiceImpl();

    @GetMapping("/getRecOrderList")
    public Result getOrderList(String usrID){
        return Result.success(recievedOrderService.getList(usrID));
    }

    @GetMapping("/getRecOrderProductId")
    public Result getRecOrderProductId(String ProductId){
        System.out.println("productId:"+ProductId);
        return Result.success(recievedOrderService.getRecievedOrderById(ProductId));
    }
}
