package com.universityhelper.order.recievedorder;

import java.util.List;

public interface RecievedOrderService {
   List<RecievedOrder> getList(String usrID);

   RecievedOrder getRecievedOrderById(String ProductId);
}
