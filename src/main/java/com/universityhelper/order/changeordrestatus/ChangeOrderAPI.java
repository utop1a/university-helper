package com.universityhelper.order.changeordrestatus;

import com.universityhelper.common.Result;
import com.universityhelper.order.changeordrestatus.impl.ChangeOrderServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ChangeOrderAPI {
    @Resource
    ChangeOrderService changeOrderService = new ChangeOrderServiceImpl();

    @GetMapping("/changestatus")
    public Result changeStatus(int status, String productionID) {
        return Result.success(changeOrderService.changeTheStatus(status, productionID));
    }

}
