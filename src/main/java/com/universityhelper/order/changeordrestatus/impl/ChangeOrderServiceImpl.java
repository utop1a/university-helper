package com.universityhelper.order.changeordrestatus.impl;

import com.universityhelper.order.changeordrestatus.ChangeOrderService;
import com.universityhelper.order.changeordrestatus.dao.ChangeOrderStatusDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional

public class ChangeOrderServiceImpl implements ChangeOrderService {
    @Resource
    private ChangeOrderStatusDao changeOrderStatusDao;

    @Override
    public boolean changeTheStatus(int Status, String productionID) {
        if (Status == 0) {
            changeOrderStatusDao.changeStatusTo0(Status, productionID);
        }
        return changeOrderStatusDao.changeStatus(Status, productionID);

    }

    ;
}
