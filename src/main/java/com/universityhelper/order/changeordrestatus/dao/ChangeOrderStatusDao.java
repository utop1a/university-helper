package com.universityhelper.order.changeordrestatus.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ChangeOrderStatusDao {
    @Update("update production set status=#{status} where productionID=#{productionID}")
    Boolean changeStatus(int status,String productionID);

    @Update("update production set recieverID='' where productionID=#{productionID}")
    Boolean changeStatusTo0(int status,String productionID);
}
