package com.universityhelper.order.releasedorder;

import com.universityhelper.common.Result;
import com.universityhelper.order.releasedorder.impl.ReleasedOrderServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ReleasedOrderAPI {
    @Resource
    ReleasedOrderService releasedOrderService= new ReleasedOrderServiceImpl();

    @GetMapping("/getRelOrderList")
    public Result getOrderList(String usrID){
        return Result.success(releasedOrderService.getList(usrID));
    }

    @GetMapping("/deleteRelOrderList")
    public Result deleteOrderList(String productionID) {return Result.success(releasedOrderService.deleteOrder(productionID));}
}

