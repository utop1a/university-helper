package com.universityhelper.order.releasedorder;

import java.util.List;

public interface ReleasedOrderService {
    List<ReleasedOrder> getList(String usrID);
    boolean deleteOrder (String productionID);
}