package com.universityhelper.order.releasedorder.dao;

import com.universityhelper.order.releasedorder.ReleasedOrder;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ReleasedOrderDao {
    @Select("select productionID,title from production where userID=#{usrID}")
    List<ReleasedOrder> getOrderByID(String usrID);

    @Select("select image from image where productionID=#{productionID} limit 1")
    String getImage(String productionID);

    @Delete("delete from production where productionID=#{productionID}")
    Boolean deleteProduction(String productionID);
}