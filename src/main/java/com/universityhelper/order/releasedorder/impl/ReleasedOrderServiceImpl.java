package com.universityhelper.order.releasedorder.impl;

import com.universityhelper.order.releasedorder.ReleasedOrder;
import com.universityhelper.order.releasedorder.ReleasedOrderService;
import com.universityhelper.order.releasedorder.dao.ReleasedOrderDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class ReleasedOrderServiceImpl implements ReleasedOrderService {
    @Resource
    private ReleasedOrderDao releasedOrderDao;

    @Override
    public List<ReleasedOrder> getList(String usrID) {
        List<ReleasedOrder> orderList= releasedOrderDao.getOrderByID(usrID);
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(releasedOrderDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public boolean deleteOrder(String productionID) {
        releasedOrderDao.deleteProduction(productionID);
        return true;
    }
}
