package com.universityhelper.user.change;

import com.universityhelper.common.Result;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserChangeAPI {
    @Resource
    UserChangeService userChangeService;

    @PostMapping("/user/change")
    public Result Change(@RequestBody UserChange userChange){
        boolean changeMessageList=userChangeService.changeList(userChange);
        return Result.success();
    }
}
