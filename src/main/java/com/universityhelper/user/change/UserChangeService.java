package com.universityhelper.user.change;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserChangeService {

    boolean changeList(UserChange userChange);
}
