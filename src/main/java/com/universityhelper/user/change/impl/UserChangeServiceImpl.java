package com.universityhelper.user.change.impl;

import com.universityhelper.user.change.UserChange;
import com.universityhelper.user.change.UserChangeService;
import com.universityhelper.user.change.dao.UserChangeDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class UserChangeServiceImpl implements UserChangeService {
    @Resource
    private UserChangeDao userChangeDao;

    @Override
    public boolean changeList(UserChange userChange) {
        return userChangeDao.changeList(userChange);
    }
}
