package com.universityhelper.user.change.dao;

import com.universityhelper.user.change.UserChange;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserChangeDao {

    @Update("Update user set userName=#{userName},gender=#{gender},age=#{age} where userID=#{userID};")
    boolean changeList(UserChange userChange);
}
