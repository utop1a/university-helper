package com.universityhelper.user.collect.dao;

import com.universityhelper.home.OrderDto;
import com.universityhelper.user.collect.UserCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserCollectDao {

    @Select("select image from image where productionID=#{productionID} limit 1")
    String getImage(String productionID);

    @Select("select price,title,production.productionID,description from production,collect where collect.userID=#{userID} and collect.productionID=production.productionID order by collect.time desc")
    List<OrderDto> getAllOrder(String userID);

    @Select("select price,title,production.productionID,description from production,collect where collect.userID=#{userID} and collect.productionID=production.productionID and category=#{category} order by collect.time desc")
    List<OrderDto> getCategoryOrder(UserCollect userCollect);
}
