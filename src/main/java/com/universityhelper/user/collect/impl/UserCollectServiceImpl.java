package com.universityhelper.user.collect.impl;

import com.universityhelper.home.OrderDto;
import com.universityhelper.user.collect.UserCollect;
import com.universityhelper.user.collect.UserCollectService;
import com.universityhelper.user.collect.dao.UserCollectDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class UserCollectServiceImpl implements UserCollectService {
    @Resource
    private UserCollectDao userCollectDao;

    @Override
    public List<OrderDto> getAllOrder(String userID) {
        List<OrderDto> orderList=userCollectDao.getAllOrder(userID);
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(userCollectDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public List<OrderDto> getCategoryOrder(UserCollect userCollect) {
        List<OrderDto> orderList=userCollectDao.getCategoryOrder(userCollect);
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(userCollectDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

}
