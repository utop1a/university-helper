package com.universityhelper.user.collect;

import com.universityhelper.home.OrderDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserCollectService {

    List<OrderDto> getAllOrder(String userID) ;
    List<OrderDto> getCategoryOrder(UserCollect userCollect);
}
