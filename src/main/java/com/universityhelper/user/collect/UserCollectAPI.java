package com.universityhelper.user.collect;


import com.universityhelper.common.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserCollectAPI {
    @Resource
    UserCollectService userCollectService;


    @GetMapping("/collect/all")
    public Result getAllOrder(String userID){
        return Result.success(userCollectService.getAllOrder(userID));
    }
    @GetMapping("/collect/category")
    public Result getCategoryOrder(UserCollect userCollect){
        return Result.success(userCollectService.getCategoryOrder(userCollect));
    }

}
