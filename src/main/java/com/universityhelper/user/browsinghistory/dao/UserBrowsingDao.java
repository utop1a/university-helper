package com.universityhelper.user.browsinghistory.dao;

import com.universityhelper.home.OrderDto;
import com.universityhelper.user.browsinghistory.UserBrowsing;
import com.universityhelper.user.collect.UserCollect;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserBrowsingDao {

    @Select("select image from image where productionID=#{productionID} limit 1")
    String getImage(String productionID);

    @Select("select price,title,production.productionID,description from production,browsingHistory where browsingHistory.userID=#{userID} and browsingHistory.productionID=production.productionID order by browsingHistory.time desc")
    List<OrderDto> getAllOrder(String userID);

    @Select("select price,title,production.productionID,description from production,browsingHistory where browsingHistory.userID=#{userID} and browsingHistory.productionID=production.productionID and category=#{category} order by browsingHistory.time desc")
    List<OrderDto> getCategoryOrder(UserCollect userCollect);

}
