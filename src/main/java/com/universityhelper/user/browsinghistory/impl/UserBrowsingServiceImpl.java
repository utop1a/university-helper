package com.universityhelper.user.browsinghistory.impl;

import com.universityhelper.home.OrderDto;
import com.universityhelper.user.browsinghistory.UserBrowsing;
import com.universityhelper.user.browsinghistory.UserBrowsingService;
import com.universityhelper.user.browsinghistory.dao.UserBrowsingDao;
import com.universityhelper.user.collect.UserCollect;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.Resource;
import java.util.List;
@Service
@Transactional
public class UserBrowsingServiceImpl implements UserBrowsingService {
    @Resource
    private UserBrowsingDao userBrowsingDao;

    @Override
    public List<OrderDto> getAllOrder(String userID) {
        List<OrderDto> orderList=userBrowsingDao.getAllOrder(userID);
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(userBrowsingDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }

    @Override
    public List<OrderDto> getCategoryOrder(UserCollect userCollect) {
        List<OrderDto> orderList=userBrowsingDao.getCategoryOrder(userCollect);
        for(int i=0;i<orderList.size();i++) {
            orderList.get(i).setImage(userBrowsingDao.getImage(orderList.get(i).getProductionID()));
        }
        return orderList;
    }
}
