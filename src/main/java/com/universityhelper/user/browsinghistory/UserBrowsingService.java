package com.universityhelper.user.browsinghistory;

import com.universityhelper.home.OrderDto;
import com.universityhelper.user.collect.UserCollect;

import java.util.List;

public interface UserBrowsingService {

    List<OrderDto> getAllOrder(String userID) ;
    List<OrderDto> getCategoryOrder(UserCollect userCollect);

}
