package com.universityhelper.user.browsinghistory;

import com.universityhelper.common.Result;
import com.universityhelper.user.collect.UserCollect;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserBrowsingAPI {
    @Resource
    UserBrowsingService userBrowsingService;

    @GetMapping("/browsing/all")
    public Result getAllOrder(String userID){
        return Result.success(userBrowsingService.getAllOrder(userID));
    }
    @GetMapping("/browsing/category")
    public Result getCategoryOrder(UserCollect userCollect){
        return Result.success(userBrowsingService.getCategoryOrder(userCollect));
    }
}
