package com.universityhelper.user.usermessage;

import com.universityhelper.common.Result;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

@RestController
public class UserMessageAPI {
    @Resource
    UserMessageService userMessageService;

    @GetMapping("/user/message")
    public Result User(String userID){
        return Result.success(userMessageService.findMessageList(userID));
    }
}
