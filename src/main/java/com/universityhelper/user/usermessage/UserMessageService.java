package com.universityhelper.user.usermessage;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMessageService {

    List<UserMessage> findMessageList(String userId);
}
