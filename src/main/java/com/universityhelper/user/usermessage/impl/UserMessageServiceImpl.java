package com.universityhelper.user.usermessage.impl;

import com.universityhelper.user.usermessage.UserMessage;
import com.universityhelper.user.usermessage.UserMessageService;
import com.universityhelper.user.usermessage.dao.UserMessageDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class UserMessageServiceImpl implements UserMessageService {

    @Resource
    private UserMessageDao userMessageDao;

    @Override
    public List<UserMessage> findMessageList(String userID) {
        return userMessageDao.findMessage(userID);
    }
}
