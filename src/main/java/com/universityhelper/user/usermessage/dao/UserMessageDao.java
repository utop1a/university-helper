package com.universityhelper.user.usermessage.dao;

import com.universityhelper.user.usermessage.UserMessage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMessageDao {

    @Select("Select userName,gender,age,realName,avatar from user where userID=#{user_id} ")
    List<UserMessage> findMessage(String u_id);

}
