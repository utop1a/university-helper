package com.universityhelper.demand;

import com.universityhelper.common.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/demand")
public class DemandAPI {

    @Resource
    private DemandService demandService;

    @GetMapping("/addDemand")
    public Result addDemand(Demand d){
        demandService.addDemand(d);
        Integer  id = demandService.DemandID();
        System.out.println(d.toString());
        System.out.println(d.getPicture().toString());
        return Result.success((Object) id.toString());
    }
}
