package com.universityhelper.demand.dao;

import com.universityhelper.demand.Demand;

public interface DemandDao {
    void addDemand1(String title,double price,String  description,String category,String userID);
    void addDemand2(Integer productionID,String picture);
    Integer DemandID();
}
