package com.universityhelper.demand;

import java.util.List;

public class Demand {

    private String price;
    private String description;
    private String category;
    private String userID;
    private String title;
    private List<String> picture;

    public Demand(String price, String description, String category, String userID, String title, List<String> picture) {
        this.price = price;
        this.description = description;
        this.category = category;
        this.userID = userID;
        this.title = title;
        this.picture = picture;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getPicture() {
        return picture;
    }

    public void setPicture(List<String> picture) {
        this.picture = picture;
    }
}
