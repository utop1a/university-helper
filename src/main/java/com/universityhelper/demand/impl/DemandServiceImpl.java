package com.universityhelper.demand.impl;

import com.universityhelper.demand.Demand;
import com.universityhelper.demand.DemandService;
import com.universityhelper.demand.dao.DemandDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DemandServiceImpl implements DemandService {
    @Resource
    private DemandDao dao;

    @Override
    public Integer DemandID() {
        return dao.DemandID();
    }

    @Override
    public void addDemand(Demand demand) {
        dao.addDemand1(demand.getTitle(),Double.parseDouble(demand.getPrice()),demand.getDescription(),demand.getCategory(),demand.getUserID());
//        Integer productionID = dao.DemandID();
//        for (String picture : demand.getPicture()) {
//            System.out.println("picture1:"+picture);
//            Pattern p=Pattern.compile("\"(?<pic>.*?)\"");
//            Matcher m=p.matcher(picture);
//            int i=0;
//            while(m.find()) {
//                picture=picture.replace(m.group(),""+(i++));
//                System.out.println("picture2:"+m.group("pic"));
//                picture=m.group("pic");
//            }
//            dao.addDemand2(productionID,picture);
//        }
    }
}
